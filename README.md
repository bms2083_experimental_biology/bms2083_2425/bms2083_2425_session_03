# BMS2083_2425_Session_02


## Descriptive statistics

Axel Nohturfft  
*11-Oct-2024*  


### Learning topics  

* Descriptive statistics functions in R  
* 'factor' data type in R  
* Summarising data frames  
* Plotting summary statistics: ggpubr package  


### To create a copy of this RStudio project follow these steps:

1. Open RStudio  
2. File Menu > New Project > Version Control > Git  
3. Paste this Gitlab URL into the "Repository URL" field: https://gitlab.surrey.ac.uk/bms2083_experimental_biology/bms2083_2425/bms2083_2425_session_03
4. Press tab key ("Project directory name" field will be filled automatically)  
5. Choose a suitable folder in the "Create project as subdirectory of..." field  
6. Click the "Create project" button  
7. Wait for setup process to complete  
8. Open the first Rmd script ...  

=======

## Learning topics  

* Descriptive statistics functions in R  
* Plotting summary statistics: ggpubr package


## To create a copy of this RStudio project follow these steps:

1. Open RStudio  
2. File Menu > New Project > Version Control > Git  
3. Paste this Gitlab URL into the "Repository URL" field: https://gitlab.surrey.ac.uk/bms2083_experimental_biology/bms2083_2425/bms2083_2425_session_03
4. Press tab key ("Project directory name" field will be filled automatically)  
5. Choose a suitable folder in the "Create project as subdirectory of..." field  
6. Click the "Create project" button  
7. Wait for setup process to complete  
8. Open the first Rmd script ...  

>>>>>>> 0db56b180dfe33229adb03bab0be9f6c31021aef
